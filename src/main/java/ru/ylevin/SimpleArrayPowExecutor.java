package ru.ylevin;

/**
 * Created by ylevin on 7/5/14.
 */
public class SimpleArrayPowExecutor implements ArrayPowExecutor {
    @Override
    public double[] pow(double[] a, double b) {
        double [] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = Math.pow(a[i], b);
        }
        return result;
    }
}
