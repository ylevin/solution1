package ru.ylevin;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ylevin on 7/5/14.
 */
public class DistributedArrayPowExecutor2 implements ArrayPowExecutor {

    final int threadAmount;
    Thread[] executors;

    public DistributedArrayPowExecutor2() {
        threadAmount = Runtime.getRuntime().availableProcessors();
        executors = new Thread[threadAmount - 1];
    }

    @Override
    public double[] pow(double[] a, double b) {
        double result[] = new double[a.length];

        int blockSize = a.length / threadAmount;

        for (int i = 0; i < threadAmount - 1; i++) {
            int offset = i * blockSize;
            executors[i] = new Thread(new Task(a, b, result, offset, offset + blockSize));
            executors[i].start();
        }
        new Task(a, b, result, (threadAmount - 1) * blockSize, a.length).run();

        for (Thread executor : executors) {
            try {
                executor.join();
            } catch (InterruptedException e) {
                /*Do Nothing*/
            }
        }

        return result;
    }

    private class Task implements Runnable {
        double[] result;
        double[] a;
        double b;

        int offset;
        int limit;

        private Task(double[] a, double b, double[] result, int offset, int limit) {
            this.result = result;
            this.a = a;
            this.b = b;
            this.offset = offset;
            this.limit = limit;
        }

        @Override
        public void run() {
            for (int j = offset; j < limit; j++) {
                result[j] = Math.pow(a[j], b);
            }
        }
    }
}
