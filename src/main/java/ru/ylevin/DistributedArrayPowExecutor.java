package ru.ylevin;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ylevin on 7/5/14.
 */
public class DistributedArrayPowExecutor implements ArrayPowExecutor {

    final int threadAmount;
    Thread[] executors;

    final int blockSize;

    public DistributedArrayPowExecutor(int blockSize) {
        this.blockSize = blockSize;
        threadAmount = Runtime.getRuntime().availableProcessors();
        executors = new Thread[threadAmount - 1];
    }

    @Override
    public double[] pow(double[] a, double b) {
        double result[] = new double[a.length];
        AtomicInteger ai = new AtomicInteger(0);
        Task task = new Task(a, b, result, ai);
        for (int i = 0; i < threadAmount - 1; i++) {
            executors[i] = new Thread(task);
            executors[i].start();
        }
        task.run();

        for (Thread executor : executors) {
            try {
                executor.join();
            } catch (InterruptedException e) {
                /*Do Nothing*/
            }
        }

        return result;
    }

    private class Task implements Runnable {
        double[] result;
        double[] a;
        double b;

        AtomicInteger index;

        private Task(double[] a, double b, double[] result, AtomicInteger index) {
            this.result = result;
            this.a = a;
            this.b = b;
            this.index = index;
        }

        @Override
        public void run() {
            int i = index.getAndAdd(blockSize);
            while (i < a.length) {
                int limit = Math.min(i + blockSize, a.length);
                for (int j = i; j < limit; j++) {
                    result[j] = Math.pow(a[j], b);
                }
                i = index.getAndAdd(blockSize);
            }
        }
    }
}
